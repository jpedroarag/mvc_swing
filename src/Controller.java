
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller implements ActionListener {

    private Modelo modelo;
    private View view;

    public Controller(Modelo modelo, View view) {
        this.modelo = modelo;
        this.view = view;
        this.view.getBotao().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {  //quando a ação de alguem for feita vai chamar este método > verifica a ação do evento
        if (e.getSource() == this.view.getBotao()) {
            this.view.displayNome();
        }
    }
}
