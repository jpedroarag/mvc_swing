
import java.awt.Label;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;


public class View extends JFrame  {  //janela

    private JTextField texto = new JTextField();
    private JButton botao = new JButton("Pronto");
    private Label label = new Label();
    private Modelo modelo;
    //tudo isso vai estar na janela

    public View(Modelo modelo) {
        //View vai ser uma janela
        super("Digite seu nome!");
        this.modelo = modelo;
        this.montaJanela();
        this.setSize(640,180);
        this.setVisible(true);
    }

    private void montaJanela() {
        texto.setVisible(true);
        texto.setSize(640,20);
        this.getContentPane().add(texto);
        
        botao.setVisible(true);
        botao.setBounds(0, 28, 640, 20);
        this.getContentPane().add(botao);
        
        label.setVisible(false);
        label.setBounds(0, 56, 640, 20);
        this.getContentPane().add(label);
    }
    
    public void displayNome() { //seta o nome do modelo para a label que está la dentro
        modelo.setNome(this.texto.getText());
        label.setText(modelo.toString());
        label.setVisible(true);
    }

    public JButton getBotao() {
        return botao;
    }
    
    

}
