
public class Modelo {
    public String nome;
    
    public String toString(){
        return ("Olá, " + nome + "!" );
    }
    
    public String getNome(){
        return nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
}
